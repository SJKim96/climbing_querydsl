package com.example.climb_dsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClimbDslApplication {

  public static void main(String[] args) {
    SpringApplication.run(ClimbDslApplication.class, args);
  }

}
