package com.example.climb_dsl.service.impl;

import com.example.climb_dsl.dto.*;
import com.example.climb_dsl.jpo.*;
import com.example.climb_dsl.repository.*;
import com.example.climb_dsl.service.ClimbService;
import com.example.climb_dsl.utils.*;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

@Service("ClimbService")
public class ClimbServiceImpl implements ClimbService {

  private final JPAQueryFactory queryFactory;

  @Autowired
  private Clm001Repository clm001Repository;

  @Autowired
  private Clm002Repository clm002Repository;

  @Autowired
  private Clm002fRepository clm002fRepository;

  public ClimbServiceImpl(JPAQueryFactory queryFactory) {
    this.queryFactory = queryFactory;
  }

  // 클라이밍장 조회
  @Override
  public Map<String, Object> selectClimbList() {
    Map<String, Object> result = new HashMap<>();
    List<Clm001> climbList = new ArrayList<>();
    QClm001 qclm001 = QClm001.clm001;
    int code = 999;
    try {
      climbList = queryFactory.selectFrom(qclm001).fetch();
      code = 200;
    } catch (Exception e) {
      code = 999;
    } finally {
      result.put("result", climbList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 등록
  @Transactional
  @Override
  public Map<String, Object> saveClimb(String climbNm) {
    Map<String, Object> result = new HashMap<>();
    Clm001 clm001 = Clm001.builder().climbNm(climbNm).build();
    int code = 999;
    try {

      clm001Repository.save(clm001);
      code = 200;
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 삭제
  @Transactional
  @Override
  public Map<String, Object> deleteClimb(Long climbSn) {
    Map<String, Object> result = new HashMap<>();
    List<Clm002f> climbFileList = new ArrayList<>();
    QClm001 qclm001 = QClm001.clm001;
    QClm002 qclm002 = QClm002.clm002;
    QClm002f qclm002f = QClm002f.clm002f;
    int code = 999;
    try {
      climbFileList = queryFactory.selectFrom(qclm002f).where(qclm002f.climbSn.eq(climbSn)).fetch();
      for (Clm002f file : climbFileList) {
        File filePath = new File(file.getFilePath() + file.getOrgnFileNm());
        if (filePath.exists() && filePath.isFile()) {
          if (filePath.delete()) {
            queryFactory
                .delete(qclm002f)
                .where(qclm002f.climbSn.eq(file.getClimbSn())
                    .and(qclm002f.grade.eq(file.getGrade()))
                    .and(qclm002f.sn.eq(file.getSn())))
                .execute();
          } else {
            result.put("code", 202);
          }
        }
      }
      queryFactory
          .delete(qclm002)
          .where(qclm002.climbSn.eq(climbSn))
          .execute();
      queryFactory
          .delete(qclm001)
          .where(qclm001.climbSn.eq(climbSn))
          .execute();
      code = 200;
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 202;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 별 난이도 조회
  @Override
  public Map<String, Object> selectClimbGradeList(Long climbSn) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    int code = 999;
    try {
      QClm002 qclm002 = QClm002.clm002;
      QClm002f qclm002f = QClm002f.clm002f;
      climbGradeList = queryFactory
          .select(Projections.bean(ClimbGrade.class,
              qclm002.climbSn,
              qclm002.grade,
              qclm002.sn,
              qclm002.note,
              qclm002.inptDt,
              qclm002f.orgnFileNm,
              qclm002f.filePath))
          .from(qclm002)
          .leftJoin(qclm002f)
          .on(qclm002.climbSn.eq(qclm002f.climbSn),
              qclm002.grade.eq(qclm002f.grade),
              qclm002.sn.eq(qclm002f.sn))
          .where(qclm002.climbSn.eq(climbSn)).fetch();
      code = 200;
    } catch (Exception e) {
      code = 999;
    } finally {
      result.put("result", climbGradeList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 난이도 등록
  @Transactional
  @Override
  public Map<String, Object> saveClimbGrade(Clm002 clm002, MultipartFile uploadFiles) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    List<Map<String, Object>> failedList = new ArrayList<>();
    List<String> imgAllowExtensions = new ArrayList<>(Arrays.asList("jpeg", "jpg", "png", "gif"));
    QClm002 qclm002 = QClm002.clm002;
    QClm002f qclm002f = QClm002f.clm002f;
    int code = 999;
    try {
      Clm002 climbGrade = clm002Repository.save(clm002);
      if (climbGrade != null) {
        if (uploadFiles != null) {
          // 파일 등록
          FileUploader uploader1 = new FileUploader(uploadFiles, imgAllowExtensions);
          uploader1.setEvents(new FileUploader.Events() {
            @Override
            public boolean onStoreDatabase(FileUploadData uploadData) {
              boolean isStored = false;
              try {
                Clm002f fileData = Clm002f.builder().grade(climbGrade.getGrade()).build();
                fileData.setClimbSn(climbGrade.getClimbSn());
                fileData.setSn(climbGrade.getSn());
                fileData.setOrgnFileNm(uploadData.getOriginalFileName());
                fileData.setFilePath(uploadData.getSaveFilePath());

                if (queryFactory
                    .insert(qclm002f)
                    .columns(qclm002f.climbSn, qclm002f.sn, qclm002f.grade, qclm002f.orgnFileNm, qclm002f.filePath)
                    .values(fileData.getClimbSn(), fileData.getSn(), fileData.getGrade(), fileData.getOrgnFileNm(), fileData.getFilePath()).execute() > 0) { // 파일 테이블 insert
                  isStored = true;
                }
              } catch (DataAccessException e) {
                // Error
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
              }

              return isStored;
            }

            @Override
            public void onSuccess(FileUploadData successData) {
              // onSuccess
            }

            @Override
            public void onError(FileUploadData errorData) {
              // onError
            }

            @Override
            public void onFinish(List<FileUploadData> successList, List<FileUploadData> errorList,
                FileUploadType type, boolean isSuccessAll) {
              // onFinish
              errorList.forEach(fileUploadData -> {
                Map<String, Object> data = new HashMap<>();
                data.put("fileName", fileUploadData.getOriginalFileName());
                data.put("mimeType", fileUploadData.getMimeType());
                data.put("fileSize", fileUploadData.getSize());
                data.put("message", fileUploadData.getType().getMessage());
                failedList.add(data);
              });
            }
          });

          switch (uploader1.uploadAll(new File("C:\\sjKimProject\\climb\\src\\main"))) {
            case SUCCESS:
              code = 200;
              break;
            case FAILED:
              code = 208; // 파일 업로드 실패
              break;
            default:
              code = 999;
              break;
          }
        }
      }
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 999;
    } finally {
      result.put("result", climbGradeList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 이름 변경
  public Map<String, Object> updateClimb(Long climbSn, String climbNm) {
    Map<String, Object> result = new HashMap<>();
    int code = 999;
    QClm001 qclm001 = QClm001.clm001;
    try {
      if (queryFactory
          .update(qclm001)
          .where(qclm001.climbSn.eq(climbSn))
          .set(qclm001.climbNm, climbNm)
          .execute() > 0) {
        code = 200;
      } else {
        code = 201;
      }

    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 난이도 수정
  @Transactional
  @Override
  public Map<String, Object> updateClimbGrade(Clm002 clm002, String deleteYn, MultipartFile uploadFiles) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    List<Map<String, Object>> failedList = new ArrayList<>();
    Clm002 climbGrade = new Clm002();
    List<String> imgAllowExtensions = new ArrayList<>(Arrays.asList("jpeg", "jpg", "png", "gif"));
    QClm002f qclm002f = QClm002f.clm002f;

    int code = 999;
    try {
      if (uploadFiles != null || deleteYn.equals("Y")) {
        Clm002f climbFile =
            queryFactory
                .selectFrom(qclm002f)
                .where(qclm002f.climbSn.eq(clm002.getClimbSn())
                    .and(qclm002f.grade.eq(clm002.getGrade()))
                    .and(qclm002f.sn.eq(clm002.getSn()))
                )
                .fetchOne();
        if (climbFile != null) {
          File filePath = new File(climbFile.getFilePath() + climbFile.getOrgnFileNm());
          if (filePath.exists() && filePath.isFile()) {
            if (filePath.delete()) {
              queryFactory
                  .delete(qclm002f)
                  .where(qclm002f.climbSn.eq(climbFile.getClimbSn())
                      .and(qclm002f.grade.eq(climbFile.getGrade()))
                      .and(qclm002f.sn.eq(climbFile.getSn()))
                  )
                  .execute();
            } else {
              result.put("code", 202);
            }
          }
        }
      }
      climbGrade = clm002Repository.save(clm002);
      if (climbGrade != null) {
        if (uploadFiles != null) {
          // 파일 등록
          FileUploader uploader1 = new FileUploader(uploadFiles, imgAllowExtensions);
          Clm002 finalClimbGrade = climbGrade;
          uploader1.setEvents(new FileUploader.Events() {
            @Override
            public boolean onStoreDatabase(FileUploadData uploadData) {
              boolean isStored = false;
              try {
                Clm002f fileData = Clm002f.builder().grade(finalClimbGrade.getGrade()).build();
                fileData.setClimbSn(finalClimbGrade.getClimbSn());
                fileData.setSn(finalClimbGrade.getSn());
                fileData.setOrgnFileNm(uploadData.getOriginalFileName());
                fileData.setFilePath(uploadData.getSaveFilePath());

                if (queryFactory
                    .insert(qclm002f)
                    .columns(qclm002f.climbSn, qclm002f.sn, qclm002f.grade, qclm002f.orgnFileNm, qclm002f.filePath)
                    .values(fileData.getClimbSn(), fileData.getSn(), fileData.getGrade(), fileData.getOrgnFileNm(), fileData.getFilePath())
                    .execute() > 0) { // 파일 테이블 insert
                  isStored = true;
                }
              } catch (DataAccessException e) {
                // Error
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
              }

              return isStored;
            }

            @Override
            public void onSuccess(FileUploadData successData) {
              // onSuccess
            }

            @Override
            public void onError(FileUploadData errorData) {
              // onError
            }

            @Override
            public void onFinish(List<FileUploadData> successList, List<FileUploadData> errorList,
                FileUploadType type, boolean isSuccessAll) {
              // onFinish
              errorList.forEach(fileUploadData -> {
                Map<String, Object> data = new HashMap<>();
                data.put("fileName", fileUploadData.getOriginalFileName());
                data.put("mimeType", fileUploadData.getMimeType());
                data.put("fileSize", fileUploadData.getSize());
                data.put("message", fileUploadData.getType().getMessage());
                failedList.add(data);
              });
            }
          });

          switch (uploader1.uploadAll(new File("C:\\sjKimProject\\climb\\src\\main"))) {
            case SUCCESS:
              code = 200;
              break;
            case FAILED:
              code = 208; // 파일 업로드 실패
              break;
            default:
              code = 999;
              break;
          }
        }
      }
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("result", climbGrade);
      result.put("code", code);
    }
    return result;
  }

  // 클라이밍장 난이도 삭제
  @Transactional
  @Override
  public Map<String, Object> deleteClimbGrade(Clm002 clm002) {
    Map<String, Object> result = new HashMap<>();
    QClm002 qclm002 = QClm002.clm002;
    QClm002f qclm002f = QClm002f.clm002f;
    int code = 999;
    try {
      Clm002f climbFile =
        queryFactory
          .selectFrom(qclm002f)
          .where(qclm002f.climbSn.eq(clm002.getClimbSn())
            .and(qclm002f.grade.eq(clm002.getGrade()))
            .and(qclm002f.sn.eq(clm002.getSn()))
          )
          .fetchOne();
      if (climbFile != null) {
        File filePath = new File(climbFile.getFilePath() + climbFile.getOrgnFileNm());
        if (filePath.exists() && filePath.isFile()) {
          if (filePath.delete()) {
            queryFactory
              .delete(qclm002f)
              .where(qclm002f.climbSn.eq(climbFile.getClimbSn())
                .and(qclm002f.grade.eq(climbFile.getGrade()))
                .and(qclm002f.sn.eq(climbFile.getSn()))
              )
              .execute();
          } else {
            result.put("code", 202);
          }
        }
      }
      queryFactory
          .delete(qclm002)
          .where(qclm002.climbSn.eq(climbFile.getClimbSn())
            .and(qclm002.grade.eq(climbFile.getGrade()))
            .and(qclm002.sn.eq(climbFile.getSn()))
          )
          .execute();
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 202;
    } finally {
      result.put("code", code);
    }
    return result;
  }
}
