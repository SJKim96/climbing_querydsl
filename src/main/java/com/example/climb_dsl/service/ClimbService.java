package com.example.climb_dsl.service;

import com.example.climb_dsl.jpo.Clm002;
import java.util.Map;
import org.springframework.web.multipart.MultipartFile;

public interface ClimbService {
  Map<String, Object> selectClimbList();
  Map<String, Object> saveClimb(String climbNm);
  Map<String, Object> deleteClimb(Long climbSn);
  Map<String, Object> selectClimbGradeList(Long climbSn);
  Map<String, Object> saveClimbGrade(Clm002 clm002, MultipartFile uploadFiles);
  Map<String, Object> updateClimb(Long climbSn, String climbNm);
  Map<String, Object> updateClimbGrade(Clm002 clm002, String deleteYn, MultipartFile uploadFiles);
  Map<String, Object> deleteClimbGrade(Clm002 clm002);
}
