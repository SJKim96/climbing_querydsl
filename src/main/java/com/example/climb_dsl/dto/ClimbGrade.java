package com.example.climb_dsl.dto;

import com.querydsl.core.annotations.QueryProjection;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import java.time.LocalDate;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Data
public class ClimbGrade {

  @Column(name = "climb_sn")
  private Long climbSn;

  @Column(name = "grade")
  private String grade;

  @Column(name = "sn")
  private Integer sn;

  @Column(name = "note")
  private String note;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Column(name = "orgn_file_nm")
  private String orgnFileNm;

  @Column(name = "file_path")
  private String filePath;

  @QueryProjection
  public ClimbGrade(Long climbSn, String grade, Integer sn, String note, LocalDate inptDt, String orgnFileNm, String filePath) {
    this.climbSn = climbSn;
    this.grade = grade;
    this.sn = sn;
    this.note = note;
    this.inptDt = inptDt;
    this.orgnFileNm = orgnFileNm;
    this.filePath = filePath;
  }
}
