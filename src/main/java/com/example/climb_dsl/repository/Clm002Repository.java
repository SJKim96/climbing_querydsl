package com.example.climb_dsl.repository;

import com.example.climb_dsl.dto.*;
import com.example.climb_dsl.jpo.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface Clm002Repository extends JpaRepository<Clm002, ClimbDtl> {

  @Transactional
  @Modifying
  @Query("DELETE FROM Clm002 c WHERE c.climbSn = :climbSn")
  void deleteAllByClimbSn(Long climbSn);

  @Query("SELECT NEW com.example.climb_dsl.dto.ClimbGrade(a.climbSn, a.grade, a.sn, a.note, a.inptDt, b.orgnFileNm, b.filePath)"
      + "FROM Clm002 a LEFT JOIN Clm002f b on a.climbSn = b.climbSn AND a.grade = b.grade AND a.sn = b.sn WHERE a.climbSn = :climbSn")
  List<ClimbGrade> selectClimbGradeList(@Param(value = "climbSn") Long climbSn);

  void deleteByClimbSnAndGradeAndSn(Long climbSn, String grade, Integer sn);
}
