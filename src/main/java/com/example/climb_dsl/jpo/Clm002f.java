package com.example.climb_dsl.jpo;

import com.querydsl.core.annotations.QueryEntity;
import java.io.Serializable;
import java.time.LocalDate;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@QueryEntity
@Table(name = "clm002f")
public class Clm002f implements Serializable {

  @Id
  @Column(name = "climb_sn")
  private Long climbSn;

  @Column(name = "grade")
  private String grade;

  @Column(name = "sn")
  private Integer sn;

  @Column(name = "orgn_file_nm")
  private String orgnFileNm;

  @Column(name = "file_path")
  private String filePath;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Builder
  public Clm002f(String grade) {
    this.grade = grade;
    this.inptDt = LocalDate.now();
  }
}
