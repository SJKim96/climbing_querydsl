package com.example.climb_dsl.jpo;

import com.querydsl.core.annotations.QueryEntity;
import java.io.Serializable;
import java.time.LocalDate;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@QueryEntity
@Table(name = "clm002")
@IdClass(ClimbDtl.class)
public class Clm002 implements Serializable {

  @Id
  @Column(name = "climb_sn")
  private Long climbSn;

  @Id
  @Column(name = "grade")
  private String grade;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "sn")
  private Integer sn;

  @Column(name = "note")
  private String note;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Builder
  public Clm002(String grade, String note) {
    this.grade = grade;
    this.note = note;
    this.inptDt = LocalDate.now();
  }
}
