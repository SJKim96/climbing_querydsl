package com.example.climb_dsl.jpo;

import com.querydsl.core.annotations.QueryEntity;
import jakarta.persistence.*;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@QueryEntity
@Table(name = "clm001")
public class Clm001 {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "climb_sn")
  private Long climbSn;

  @Column(name = "climb_nm")
  private String climbNm;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Builder
  public Clm001(String climbNm) {
    this.climbNm = climbNm;
    this.inptDt = LocalDate.now();
  }
}
