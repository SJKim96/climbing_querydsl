package com.example.climb_dsl.jpo;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClimbDtl implements Serializable {
  private long climbSn;
  private String grade;
  private Integer sn;

}
